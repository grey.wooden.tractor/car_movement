#include <vector>
#include <memory>
#include <algorithm>

#include "cars.h"
#include "CarBuilder.h"
#include "const_params.h"


bool main_loop(std::unique_ptr<CarBuilder> &builder, std::vector<Car *> &cars) {

    for (auto &car1 : cars) {
        for (auto car2 : cars) {
            if (car1 == car2)
                continue;

            if (!car1->getFuturePos().intersects(car2->getFuturePos())) { // if two cars not intersect with each other
                if (!car1->needPassOtherCar(car2))
                    car1->moveCar();
            } else {
                car2->moveCar();
            }
        }

        if (car1->rect.pos.x <= 0
            || car1->rect.pos.x >= SCREEN_WIDTH
            || car1->rect.pos.y <= 0
            || car1->rect.pos.y >= SCREEN_HEIGHT) {

//            delete car1;
            car1 = nullptr;
            cars.erase(std::remove(cars.begin(), cars.end(), nullptr), cars.end());

            //            spawnCar();
            Car &car = builder->create().spawnCar().getResult();
            cars.push_back(&car);
        }
    }
    return main_loop(builder, cars);
}

int main(int argc, char **argv) {
    std::vector<Car *> cars;
    std::unique_ptr<CarBuilder> builder = std::make_unique<CarBuilder>();

    for (auto i = 0; i < INITIAL_CARS_COUNT; ++i) {
        Car &car = builder->create().spawnCar().getResult();
        cars.push_back(&car);
    }
    main_loop(builder, cars);

    return 0;
}