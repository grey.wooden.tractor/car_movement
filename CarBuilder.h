#ifndef CAR_GAME_CARBUILDER_H
#define CAR_GAME_CARBUILDER_H

class CarBuilder {
public:
    CarBuilder() {}

    CarBuilder &create();

    CarBuilder &spawnCar();

    Car &getResult();
private:
    Car *car;

    CarBuilder &spawnCarFromTop();

    CarBuilder &spawnCarFromBot();

    CarBuilder &spawnCarFromLeft();

    CarBuilder &spawnCarFromRight();

    CarBuilder &setSpeed(int speed);

    CarBuilder &setRectPos(sPos pos);

    CarBuilder &setRectSize(sSize size);

    CarBuilder &setDirection(eDirection dir);
};

#endif //CAR_GAME_CARBUILDER_H
