#ifndef CAR_GAME_CARS2_H
#define CAR_GAME_CARS2_H

#include <cstdlib>
#include "basic_structs.h"

class Car {
public:
    sRect rect;
    eDirection dir;
    int speed;

    void moveCar();

    sRect getFuturePos();

    bool needPassOtherCar(Car *otherCar);

    virtual int getFuel() = 0;

    virtual void refill(int count) = 0;
};


class GasEngineCar : public virtual Car {
public:
    int fuel;

    int getFuel() override;

    void refill(int count) override;

    void moveCar();
};

class ElectroCar : public virtual Car {
public:
    int charge;

    int getFuel() override;

    void refill(int count) override;

    void moveCar();
};

class HybridCar : public virtual GasEngineCar, public virtual ElectroCar {
public:
    int getFuel() override;

    void refill(int count) override;

    void moveCar();
};



#endif //CAR_GAME_CARS2_H
