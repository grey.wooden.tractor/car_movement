#ifndef CAR_GAME_REAL_RANDOM_H
#define CAR_GAME_REAL_RANDOM_H

#include <random>

static std::random_device rd;
static std::mt19937 rng(rd());

static int real_rand(int max) {
    std::uniform_int_distribution<int> uni(0, max);
    return uni(rng);
}

#endif // CAR_GAME_REAL_RANDOM_H