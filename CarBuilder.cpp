#include "cars.h"
#include "CarBuilder.h"
#include "real_random.h"
#include "const_params.h"
#include <memory>


CarBuilder &CarBuilder::create() {
    int rand = real_rand(2);

    if (rand % 3 == 0) {
        car = new GasEngineCar();
    } else if (rand % 3 == 1) {
        car = new ElectroCar();
    } else { // if (carType % 3 == 2)
        car = new HybridCar();
    }
    return *this;
}

CarBuilder& CarBuilder::spawnCarFromTop() {
    setRectPos(sPos{SCREEN_WIDTH / 2, 0});
    setRectSize(sSize{CAR_WIDTH, CAR_HEIGHT});
    setSpeed(1);
    setDirection(eDirection::DOWN);

    return *this;
}

CarBuilder& CarBuilder::spawnCarFromBot() {
    setRectPos(sPos{SCREEN_WIDTH / 2, SCREEN_HEIGHT});
    setRectSize(sSize{CAR_WIDTH, CAR_HEIGHT});
    setSpeed(1);
    setDirection(eDirection::UP);

    return *this;
}

CarBuilder& CarBuilder::spawnCarFromLeft() {
    setRectPos(sPos{0, SCREEN_HEIGHT / 2});
    setRectSize(sSize{CAR_WIDTH, CAR_HEIGHT});
    setSpeed(1);
    setDirection(eDirection::RIGHT);

    return *this;
}

CarBuilder& CarBuilder::spawnCarFromRight() {
    setRectPos(sPos{0, SCREEN_HEIGHT / 2});
    setRectSize(sSize{CAR_WIDTH, CAR_HEIGHT});
    setSpeed(1);
    setDirection(eDirection::LEFT);

    return *this;
}

CarBuilder& CarBuilder::spawnCar() {
    int rand = real_rand(3);

    if (rand % 4 == 0) {
        return spawnCarFromRight();
    } else if (rand % 4 == 1) {
        return spawnCarFromTop();
    } else if (rand % 4 == 2) {
        return spawnCarFromBot();
    } else { // if (real_rand(3) % 4 == 3)
        return spawnCarFromLeft();
    }
}

// Params constructors
CarBuilder &CarBuilder::setSpeed(int speed) {
    car->speed = speed;
    return *this;
}

CarBuilder &CarBuilder::setRectPos(sPos pos) {
    car->rect.pos = pos;
    return *this;
}

CarBuilder &CarBuilder::setRectSize(sSize size) {
    car->rect.size = size;
    return *this;
}

CarBuilder &CarBuilder::setDirection(eDirection dir) {
    car->dir = dir;
    return *this;
};

Car &CarBuilder::getResult() {
    return *car;
}
