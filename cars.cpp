#include "cars.h"
#include "real_random.h"

void Car::moveCar() {
    switch (dir) {
        case eDirection::UP:
            rect.pos.y += speed;
        case eDirection::DOWN:
            rect.pos.y -= speed;
        case eDirection::RIGHT:
            rect.pos.x += speed;
        case eDirection::LEFT:
            rect.pos.x -= speed;
    }
}

sRect Car::getFuturePos() {
    switch (dir) {
        case eDirection::UP:
            return sRect(sPos{rect.pos.x, rect.pos.y + speed}, rect.size);
        case eDirection::DOWN:
            return sRect(sPos{rect.pos.x, rect.pos.y - speed}, rect.size);
        case eDirection::RIGHT:
            return sRect(sPos{rect.pos.x + speed, rect.pos.y}, sSize{rect.size.height, rect.size.width});
        case eDirection::LEFT:
            return sRect(sPos{rect.pos.x + speed, rect.pos.y}, sSize{rect.size.width, rect.size.height});
    }
}

bool Car::needPassOtherCar(Car *otherCar) {
    bool result = false;
    auto otherDir = otherCar->dir;
    switch (dir) {
        case eDirection::UP:
            if (otherDir == eDirection::RIGHT)
                result = true;
            break;
        case eDirection::DOWN:
            if (otherDir == eDirection::LEFT)
                result = true;
            break;
        case eDirection::RIGHT:
            if (otherDir == eDirection::DOWN)
                result = true;
            break;
        case eDirection::LEFT:
            if (otherDir == eDirection::UP)
                result = true;
            break;
    }
    return result;
}

// GasEngineCar
int GasEngineCar::getFuel() { return fuel; }

void GasEngineCar::refill(int count) { fuel += count; }

void GasEngineCar::moveCar() {
    fuel--;
    Car::moveCar();
}

// ElectroCar
int ElectroCar::getFuel() { return charge; }

void ElectroCar::refill(int count) { charge += count; }

void ElectroCar::moveCar() {
    charge--;
    Car::moveCar();
}

// HybridCar
void HybridCar::refill(int count) {
    charge += count / 2;
    fuel += count / 2;
}

int HybridCar::getFuel() { return charge + fuel; }

void HybridCar::moveCar() {
    if (real_rand(1) % 2 == 0)
        ElectroCar::moveCar();
    else
        GasEngineCar::moveCar();
}
