#ifndef CAR_GAME_BASIC_STRUCTS_H
#define CAR_GAME_BASIC_STRUCTS_H

struct sPos {
    sPos() {
        x = 0;
        y = 0;
    }

    sPos(int aX, int aY) {
        x = aX;
        y = aY;
    }

    int x;
    int y;
};

struct sSize {
    sSize() {
        width = 0;
        height = 0;
    }

    sSize(int aW, int aH) {
        width = aW;
        height = aW;
    }

    int width;
    int height;
};

struct sRect {
    sRect() {};

    sRect(sPos pos_, sSize size_) {
        pos = pos_;
        size = size_;
    }

    sPos pos;
    sSize size;

    /**
     * if *this* car intersects with the *other*
     * @param other other car
     * @return does *this* car in intersects with the *other*
     */
    bool intersects(const sRect &other) {
        return !(
                (other.pos.x + other.size.width <= pos.x) ||
                (other.pos.y + other.size.height <= pos.y) ||
                (other.pos.x >= pos.x + size.width) ||
                (other.pos.y >= pos.y + size.height)
        );
    }
};

enum class eDirection {
    UP,
    LEFT,
    RIGHT,
    DOWN
};

#endif //CAR_GAME_BASIC_STRUCTS_H
